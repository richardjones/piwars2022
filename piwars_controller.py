import board
from adafruit_motorkit import MotorKit
from approxeng.input.selectbinder import ControllerResource
from approxeng.input.controllers import ControllerNotFoundError
import math
from time import sleep

class Robot():
    def __init__(self, max_throttle):
        self.kit = MotorKit(i2c=board.I2C())
        self.max_throttle = max_throttle
        self.normal_drive = True
    def cleanup(self):
        self.stop()
    def toggle_drive_mode(self):
        self.normal_drive = not self.normal_drive
    def __adjust_throttle(self, throttle):
        throttle *= self.max_throttle
        if throttle < 0.1 and throttle > -0.1:
            return 0.0
        elif throttle > 1.0:
            return 1.0
        elif throttle < -1.0:
            return -1.0
        else:
            return throttle
    def __motor_fl(self):
        return self.kit.motor4
    def __motor_fr(self):
        return self.kit.motor2
    def __motor_bl(self):
        return self.kit.motor3
    def __motor_br(self):
        return self.kit.motor1
    def __drive_fl(self, throttle):
        self.__motor_fl().throttle = self.__adjust_throttle(throttle)
    def __drive_fr(self, throttle):
        self.__motor_fr().throttle = self.__adjust_throttle(throttle)
    def __drive_bl(self, throttle):
        self.__motor_bl().throttle = self.__adjust_throttle(throttle)
    def __drive_br(self, throttle):
        self.__motor_br().throttle = self.__adjust_throttle(throttle)
    def __normal_mixer(self, throttle, yaw):
        # Credit: https://approxeng.github.io/approxeng.input/examples/tiny4wd.html
        # throttle = forward/backward and yaw = left/right
        # Adjust the amount of forward/backward by the amount of left/right and apply to each side
        left = throttle + yaw
        right = throttle - yaw
        # Keep maximum drive relative to 1.0 (max)
        scale = 1 / max(1, abs(left), abs(right))
        return left * scale, right * scale
    def __mec_mixer(self, throttle, yaw):
        # Credit: https://compendium.readthedocs.io/en/latest/tasks/drivetrains/mecanum.html
        # fl and br power = sqrt(2)/2 * (sin A + cos A)
        # fr and bl power = sqrt(2)/2 * (sin A - cos A)
        # sin A = opposite / hypotenuse and cos A = adjacent / hypotenuse
        # Rearrange sin A + cosA = (opp / hyp) + (adj / hyp) = (opp + adj) / hyp
        # Same applies to sin A - cos A = (opp - adj) / hyp
        # opposite = throttle and adjacent = yaw
        SQRT_2_DIV_2 = 0.707106781186548 # Constant value (don't need to recalculate this every time)
        hyp = math.sqrt(throttle**2 + yaw**2) # hyp**2 = opp**2 + adj**2
        if hyp == 0:
            return 0, 0 # No forward or sideways movement
        return SQRT_2_DIV_2 * ((throttle + yaw) / hyp), SQRT_2_DIV_2 * ((throttle - yaw) / hyp)
    def stop(self):
        self.drive(0, 0)
    def drive(self, throttle, yaw):
        if self.normal_drive:
            left, right = self.__normal_mixer(throttle, yaw)
            self.__drive_fl(left)
            self.__drive_fr(right)
            self.__drive_bl(left)
            self.__drive_br(right)
        else:
            frbl, flbr = self.__mec_mixer(throttle, yaw)
            self.__drive_fl(flbr)
            self.__drive_br(flbr)
            self.__drive_fr(frbl)
            self.__drive_bl(frbl)

robot = Robot(1.0)

# Loop forever
while True:
    try: # ControllerResource raises an error if no controllers are found
        with ControllerResource() as joystick:
            # Loop until disconnected
            while joystick.connected:
                # Check for change to drive mode
                presses = joystick.check_presses()
                if presses.home:
                    robot.cleanup()
                    quit()
                if presses.l2:
                    robot.toggle_drive_mode()
                # Get the left/right and forward/backward from the joystick
                lr, fb = joystick['lx', 'ly']
                # Drive the robot
                robot.drive(throttle=fb, yaw=lr)
    except ControllerNotFoundError: # No controller bound to Pi
        sleep(1) # Idle then try again (in infinite loop)
